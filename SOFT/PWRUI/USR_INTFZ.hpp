#ifndef USR_INTFZ_HPP_INCLUDED
#define USR_INTFZ_HPP_INCLUDED

class USR_INTFZ {
private:
    const int UID0_P=5;//Encoder de usuario enter
    const int UID1_P=20;//Encoder de usuario up first
    const int UID2_P=19;//Encoder de usuario down first
    
    //Variables de detección de flancos 
bool UIDD=false;//Dirección derecha falso
bool UIDI=false;//Dirección izquierda falso

bool DCIC=false;//Ciclo de paso de encoder
bool DCIC1=false;//Ciclo 1 de paso de encoder
bool DCIC2=false;//Ciclo 2 de paso de encoder
        
bool isUID0=false;
bool wsUID0=false;

bool isUID1=false;
bool wsUID1=false;

bool isUID2=false;
bool wsUID2=false;


  //Variables de detección de flancos 
public:

    int RT_ENC=0;//Incrementos de encoder rotatorio, +-1
    int PULS=0;//Pulsación de encoder, RE=true.
    
    USR_INTFZ();//Constructor sin estado
    USR_INTFZ(const USR_INTFZ& gc);//Duplicador de clase
    USR_INTFZ& operator=(const USR_INTFZ& sm);//Igualador de clase
    ~USR_INTFZ();//Destructor
    
    void SETP();
    void MIP();//Manejo de entradas de pulsador
    void MIE();//Manejo de entradas de encoder
    
};

#endif

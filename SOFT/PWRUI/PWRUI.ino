#include "USR_INTFZ.hpp"
const int SLED = 17; // LED connected to digital pin 13

const int PWR_UP = 9; // POWER_PIN_VCC on START; POWER_DOWN GND
//const int PWR_BUTT = 5; // 
int NBP=0;

USR_INTFZ INTERF;

void setup() {
  // put your setup code here, to run once:
  INTERF=USR_INTFZ();
  INTERF.SETP();
  pinMode(SLED, OUTPUT);      // sets the digital pin 13 as output
  digitalWrite(SLED,0);
  pinMode(PWR_UP, OUTPUT);      // sets the digital pin 13 as output
  digitalWrite(PWR_UP,1);
 
}

void loop() {
  // put your main code here, to run repeatedly:
digitalWrite(PWR_UP,1);
digitalWrite(SLED,1);

  
  for(int i=0;i<100000;i++){
   INTERF.MIP();
   if(INTERF.PULS>5){
    digitalWrite(SLED,0);
    INTERF.PULS=0;
    break;
    }
    else{
      digitalWrite(SLED,1);
      }
  }

    delay(2000);
  digitalWrite(SLED,0);
delay(2000);
  digitalWrite(SLED,1);
  //POWER_DOWN

    digitalWrite(PWR_UP,0);
    delay(2000);

}
